import React from 'react'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import { BoxSearch } from '../components/BoxSearch'
import { ItemDetail } from '../components/ItemDetail'
import { ItemsResults } from '../components/ItemsResults'

export const AppRouter = () => {
    return (
        <Router>
            <div className="container">
                <Route path="/" component={ BoxSearch } />
                <Route exact path="/items/" component={ ItemsResults } />
                <Route exact path="/items/:id" component={ ItemDetail } />
            </div>
        </Router>
    )
}
