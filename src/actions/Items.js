import axios from "axios"
import { types } from "../types/types"

//const ULR_API = "http://localhost:9090/api/items";
const ULR_API = "https://test-ml-web-v2.herokuapp.com/api/items";

export const setItemQuery = (query) => {
    return {
        type: types.QUERY_ITEM,
        payload: query
    }
}

export const setItemLoading = (state) => {
    return {
        type: types.ITEM_LOADING,
        payload: state
    }
}

export const getItemsByQuery = (query) => {
    console.log(ULR_API+"?search=" + query)
    return(dispatch) => {
        dispatch(setItemLoading(true));
        axios.get(ULR_API + "?search=" + query )
            .then(response => {
                dispatch(setCategories(response.data.categories))
                dispatch(setArrayItems(response.data.items));
                dispatch(setItemLoading(false));
            } ).catch(err =>{
                dispatch(setItemLoading(false));
                console.log(err)
            });
    }
}

export const setCategories = (categories) => {
    return {
        type: types.CATEGORIES_ITEMS,
        payload: categories
    }
}

export const setArrayItems = (items) => {
    return {
        type: types.SEARCH_ITEMS,
        payload: items
    }
}

export const getItemDetail = (id) => {
    return(dispatch) => {
        dispatch(setItemLoading(true));
        axios.get(ULR_API + "/" +  id)
            .then(response => {
                dispatch(setItemLoading(false));
                dispatch(setItemDetail(response.data.item)) 
            }).catch(err =>{
                dispatch(setItemLoading(false));
                console.log(err)
            });;
        
    }
}

export const setItemDetail = (data) => {
    return {
        type: types.DETAIL_ITEMS,
        payload: data
    }
}