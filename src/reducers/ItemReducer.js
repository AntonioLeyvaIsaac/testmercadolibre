import { types } from "../types/types";

const initialState = {
    itemLoading: false,
    itemQuery: '',
    arrayItems: [],
    itemCategories:[],
    itemResult: {
        id: "",
        title: "",
        price: {
            currency: "",
            amount: 0,
            decimals: ""
        },
        picture: "",
        condition: "",
        free_shipping: true,
        sold_quantity: 0,
        description: ""
    }
    
}

export const ItemReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ITEM_LOADING:
            return {
                ...state,
                itemLoading: action.payload
            }

        case types.QUERY_ITEM:
            return {
                ...state,
                itemQuery: action.payload
            }

        case types.SEARCH_ITEMS:
            return {
                ...state,
                arrayItems: action.payload
            }

        case types.DETAIL_ITEMS:
            return {
                ...state,
                itemResult: action.payload
            }

        case types.CATEGORIES_ITEMS:
            return {
                ...state,
                itemCategories: action.payload
            }
    
        default:
            return state;
    }
}