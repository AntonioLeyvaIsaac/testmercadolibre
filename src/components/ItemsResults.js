import React, { useEffect } from 'react'
import { useLocation } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import queryString from 'query-string';
import Breadcrumb from './Breadcrumb'
import CardItem from './CardItem'
import { getItemsByQuery, setItemQuery } from '../actions/Items';

export const ItemsResults = () => {
    const {arrayItems, itemCategories, itemLoading} = useSelector( state => state.item );
    const location = useLocation();
    const { search = '' } = queryString.parse( location.search );

    const dispatch = useDispatch();

    useEffect(() => { 
        dispatch(getItemsByQuery(search)); 
        dispatch(setItemQuery(search))
    }, []);
    
    return (
        <div className="ml-container-responsive">
            {itemLoading ? <div className="progress-line"></div> :
                <div className="ml-grid-columns">
                    <Breadcrumb 
                        itemCategories={itemCategories}
                    />
                    <div className="ml-container-general">
                    {
                        arrayItems.map((item, index) => (
                            <CardItem 
                                key={index}
                                item={item}
                            />
                        ))
                    }
                            
                    </div>
                </div>
            }
        </div>
    )
}
