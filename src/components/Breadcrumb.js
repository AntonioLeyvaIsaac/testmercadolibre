import React from 'react'

export default function Breadcrumb({itemCategories}) {
    return (
        <div className="ml-container-breadcrumb">
            <ul className="ml-breadcrumb-path">
                {
                    itemCategories.map((categorie, index) => (
                        <li key={index}>{categorie}</li>
                    ))
                }
            </ul>
        </div>
    )
}

