import React from 'react'
import { useHistory } from "react-router-dom";
import IMG_SHIPPING from '../assets/ic_shipping@2x.png.png';

export default function CardItem({item}) {
    const {picture, title, price, address, free_shipping} = item;
    let history = useHistory();
    
    const priceFormat = (num) => (
        '$' + num.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    )

    const getDetail = (id) => {
        history.push(`/items/${id}`);
    }

    return (
        <div className="ml-item-result" onClick={() => getDetail(item.id)} >
            <div className="ml-item-image">
                <img src={picture} alt="img_product"/>
            </div>
            <div className="ml-item-detail">
                <div className="ml-item-price">
                    <span className="ml-price-value"> { priceFormat(price.amount) }</span>
                    {free_shipping && <img src={ IMG_SHIPPING } alt="img_shipping"/> }
                </div>
                <div className="ml-item-description">
                    <span className="ml-desciption-value">{ title }</span>
                </div>
            </div>
            <div className="ml-item-ubication">
                <span className="ml-ubication-value"> {address} </span>
            </div>
        </div>
    )
}
