import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from "react-router-dom";
import { getItemDetail } from '../actions/Items';
import Breadcrumb from './Breadcrumb'

export const ItemDetail = () => {
    const {itemResult, itemCategories, itemLoading} = useSelector( state => state.item)
    const dispatch = useDispatch();
    const { id } = useParams();
    
    useEffect(() => { 
        dispatch(getItemDetail(id)); 
    }, []);

    return (
        <div className="ml-container-responsive">
            {itemLoading ? <div className="progress-line"></div> :
                <div className="ml-grid-columns">
                    <Breadcrumb 
                        itemCategories={itemCategories}
                    />
                    <div className="ml-container-general">
                        <div className="ml-detail-information">
                            <div className="ml-detail-img">
                                <img src={itemResult.picture} alt="img_prod"/>
                            </div>
                            <div className="ml-detail-others">
                                <span >{itemResult.condition} - {itemResult.sold_quantity} vendidos</span>
                                <span >{itemResult.title}</span>
                                <span >$ {itemResult.price['amount']}</span>
                                <button>Comprar</button>
                            </div>
                        </div>
                        <div className="ml-detail-description">
                            <h2>Descripción del producto</h2>
                            <p>{itemResult.description}</p>
                        </div>
                    </div>
                </div>
            }
        </div>
    )
}
