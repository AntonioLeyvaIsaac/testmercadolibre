import React, {useEffect, useState} from 'react'
import { Link } from 'react-router-dom';
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import IMG_LOG from '../assets/Logo_ML@2x.png.png';
import IMG_SEARCH from '../assets/ic_Search@2x.png.png';
import { getItemsByQuery } from '../actions/Items';

export const BoxSearch = ({ history }) => {
    const {itemQuery} = useSelector( state => state.item );
    const dispatch = useDispatch();
    const [data, setdata] = useState(itemQuery);
    
    useEffect(() => { 
        setdata(itemQuery)
    }, []);

    const handleInputChage = (e) => {
        setdata(e.target.value)
    }

    const sendData = (e) => {
        e.preventDefault();
        dispatch(getItemsByQuery(data)); 
        history.push(`/items?search=${ data }`)
    }

    return (
        <div className="ml-bar-container">
            <div className="ml-container-responsive">
                <div className="ml-grid-columns">
                    <div className="ml-form-logoML">
                        <Link to="/">
                            <img src={IMG_LOG} alt="logo_ML"/>
                        </Link>
                    </div>
                    <form className="ml-form-container"  onSubmit={sendData}>
                        <input 
                            value={data}
                            onChange={handleInputChage}
                            className="ml-input-search"
                            name="data"
                            placeholder="Nunca dejes de buscar"
                            type="text" />
                        <span className="ml-input-icon" onClick={sendData}>
                            <img src={ IMG_SEARCH } alt="search"/>
                        </span>
                    </form>
                </div>
            </div>
        </div>
    )
}
