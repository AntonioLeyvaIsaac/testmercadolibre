import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AppRouter } from './routers/AppRouter';
import './sass/main.scss'

import { store } from './store/store';

ReactDOM.render(
  <React.StrictMode>
    <Provider store = { store }>
      <AppRouter />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);