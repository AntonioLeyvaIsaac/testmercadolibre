# WEB TEST MERCADO LIBRE

Este proyecto contiene la aplicación web con los solicitado en la prueba

## Instalación 
Descargar el proyecto del repositorio
```sh
cd testmercadolibre
npm i
npm start
```
**Note: La aplicación correrá en el puerto `3000`**

## Uso
Dentro del archivo `/src/actions/Items.js` se encuentra la constante para poder hacer uso de la API local o remota, basta con descomentar la que se requiera para su uso 

```javascript
//const ULR_API = "http://localhost:9090/api/items";
const ULR_API = "https://test-ml-web-v2.herokuapp.com/api/items";
```


Inicio

>http://localhost:3000/

Obtener listado de productos

`:query`: Producto a buscar
>http://localhost:3000/items?search=:query

Obtener detalle del producto

`:id`: ID del producto
>http://localhost:3000/items/:id

## Deployment
Se cuenta con una version de prueba desplegada en un servidor de HEROKU bajo el dominio: 
https://test-ml-web-v2.herokuapp.com/

Inicio
>https://test-ml-web-v2.herokuapp.com/

Listado de productos
>https://test-ml-web-v2.herokuapp.com/items?search=:query

Detalle del producto
>https://test-ml-web-v2.herokuapp.com/items/:id